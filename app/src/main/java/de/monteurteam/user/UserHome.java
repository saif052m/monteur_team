package de.monteurteam.user;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.monteurteam.Constants;
import de.monteurteam.Login;
import de.monteurteam.R;

public class UserHome extends AppCompatActivity {
    @BindView(R.id.userImg)
    ImageView userImg;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.userPanel)
    LinearLayout userPanel;
    @BindView(R.id.cameraImg)
    ImageView cameraImg;
    @BindView(R.id.camera)
    TextView camera;
    @BindView(R.id.cameraPanel)
    LinearLayout cameraPanel;
    @BindView(R.id.phoneImg)
    ImageView phoneImg;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.phonePanel)
    LinearLayout phonePanel;
    @BindView(R.id.cartImg)
    ImageView cartImg;
    @BindView(R.id.cart)
    TextView cart;
    @BindView(R.id.cartPanel)
    LinearLayout cartPanel;
    @BindView(R.id.calendarImg)
    ImageView calendarImg;
    @BindView(R.id.calendar)
    TextView calendar;
    @BindView(R.id.calendarPanel)
    LinearLayout calendarPanel;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.logoutImg)
    ImageView logoutImg;
    @BindView(R.id.logout)
    TextView logout;
    @BindView(R.id.logoutPanel)
    LinearLayout logoutPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);
        ButterKnife.bind(this);

        DrawableCompat.setTint(userImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));
        DrawableCompat.setTint(cameraImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));
        DrawableCompat.setTint(phoneImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));
        DrawableCompat.setTint(cartImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));
        DrawableCompat.setTint(calendarImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));
        DrawableCompat.setTint(logoutImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        }

        SharedPreferences sp = getSharedPreferences(Constants.sharedPrefs, Context.MODE_PRIVATE);
        name.setText(sp.getString("user_name", ""));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        }
    }

    @OnClick({R.id.userPanel, R.id.cameraPanel, R.id.phonePanel, R.id.cartPanel, R.id.calendarPanel, R.id.logoutPanel})
    public void onViewClicked(View view) {
        Constants.clickEffect(view);
        switch (view.getId()) {
            case R.id.userPanel:
                break;
            case R.id.cameraPanel:
                openCamera();
                break;
            case R.id.phonePanel:
                Toast.makeText(UserHome.this, "For future use!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.cartPanel:
                Toast.makeText(UserHome.this, "For future use!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.calendarPanel:
                Toast.makeText(UserHome.this, "For future use!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.logoutPanel:
                SharedPreferences sp = getSharedPreferences(Constants.sharedPrefs, Context.MODE_PRIVATE);
                sp.edit().putInt("login", 0).apply();
                Intent intent = new Intent(this, Login.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private final int WRITE_PERMISSION_CODE = 2;
    private final int CAM_CODE = 200;
    private String filePath;
    private File cameraFile;

    // call this function
    private void openCamera() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_PERMISSION_CODE);
            return;
        }
        File extDir = Environment.getExternalStorageDirectory();
        Random r = new Random();
        String name = (r.nextInt(100000000) + r.nextInt(100000000)) + ".jpg";
        cameraFile = new File(extDir, name);
        if (checkExternalStorage()) {
            String text = "writing into externanal storage";
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(cameraFile);
                fos.write(text.getBytes());
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (cameraFile.isFile()) {
                Log.e("tag", "is file");
                Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraintent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cameraFile));
                startActivityForResult(cameraintent, CAM_CODE);
            } else {
                Log.e("tag", "oops not file");
            }
        } else {
            Log.e("tag", "External storage not available!");
        }
    }

    public boolean checkExternalStorage() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("tag", "onActivityResult");

        switch (requestCode) {
            case CAM_CODE:
                if (data != null) {
                    Uri uri = null;
                    if (data.getData() == null) {
                        //from camera
                        if (cameraFile == null) {
                            Log.e("tag", "Cannot fetch photo!");
                            return;
                        }
                        uri = Uri.fromFile(cameraFile);
                    } else {
                        uri = data.getData();
                    }
                    handleCameraSelection(uri);
                } else {
                    Uri uri = null;
                    if (cameraFile == null) {
                        Log.e("tag", "unable to fetch photo!");
                        return;
                    }
                    uri = Uri.fromFile(cameraFile);
                    handleCameraSelection(uri);
                }
                break;

            default:
                break;
        }
    }

    @SuppressLint("NewApi")
    public static String getPath(Context context, Uri uri) {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public void handleCameraSelection(Uri uri) {
        Log.e("tag", "uri: " + uri);
        if (Build.VERSION.SDK_INT < 11) {
            filePath = getPath(this, uri);
            ;
            //filePath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
        } else if (Build.VERSION.SDK_INT < 19) {
            //filePath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
            filePath = getPath(this, uri);
            ;
        } else {
            filePath = getPath(this, uri);
            ;
            //filePath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
        }

        Log.e("File path", "File Path: " + filePath);

        Bitmap bmp = BitmapFactory.decodeFile(filePath);
        bmp = bmpAfterRotationCheck(bmp, filePath);
        if (bmp != null) {
            new MyTask().execute();
        } else {
            Log.e("tag", "Cannot retrieve photo!");
        }
    }

    private Bitmap bmpAfterRotationCheck(Bitmap bmp, String filePath) {
        try {
            ExifInterface exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Log.e("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true); // rotating bitmap
            return bmp;
        } catch (Exception e) {
        }
        return bmp;
    }

    public class MyTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            return uploadData(Constants.addPhoto);
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if(progressBar == null)
                return;

            progressBar.setVisibility(View.GONE);
            if(response == null) {
                Toast.makeText(UserHome.this, "Network problem!", Toast.LENGTH_SHORT).show();
                return;
            }

            try {
                JSONObject object = new JSONObject(response);
                String status = object.getString("status");
                if(status.equalsIgnoreCase("success")){
                    Toast.makeText(UserHome.this, "Photo uploaded!", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(UserHome.this, "Upload Failed!", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String uploadData(String url) {
        MultipartEntity multiPart = new MultipartEntity();
        File sourceFile = new File(filePath);
        try {
            // sending parameters
            SharedPreferences sp = getSharedPreferences(Constants.sharedPrefs, Context.MODE_PRIVATE);
            multiPart.addPart("user_id", new StringBody(sp.getString("user_id", "")));
            multiPart.addPart("date", new StringBody(new SimpleDateFormat("yyyy-MM-dd").format(System.currentTimeMillis())));
            if(sourceFile != null)
                multiPart.addPart("photo", new FileBody(sourceFile));
            String response = multipost(url, multiPart);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private static String multipost(String urlString, MultipartEntity reqEntity) {
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(30000);
            conn.setRequestMethod("POST");
            conn.setUseCaches(false);
            //conn.setChunkedStreamingMode(1024);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.setRequestProperty("connection", "Keep-Alive");
            conn.addRequestProperty("Content-length", reqEntity.getContentLength()+"");
            conn.setRequestProperty("Cache-Control", "no-cache");
            conn.addRequestProperty(reqEntity.getContentType().getName(), reqEntity.getContentType().getValue());

            OutputStream os = conn.getOutputStream();
            reqEntity.writeTo(conn.getOutputStream());
            os.close();
            conn.connect();
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return readStream(conn.getInputStream());
            }
            else{
                Log.e("tag", "code: "+conn.getResponseCode());
                Log.e("tag", "msg: "+conn.getResponseMessage());
                Log.e("tag", "cannot read stream");
            }
        }
        catch (Exception e) {
            Log.e("Uploading", "multipart post error " + e + "(" + urlString + ")");
        }
        return null;
    }

    private static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

}
