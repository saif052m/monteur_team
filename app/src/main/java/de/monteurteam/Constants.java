package de.monteurteam;

import android.os.Handler;
import android.view.View;

public class Constants {
    public static final String adminNumber = "+4915792330307";
    public static final String sharedPrefs = "monteurteam_prefs";


    public static final String adminLogin = "http://monteurverbund.com/mobile/adminLogin";
    public static final String updateAdminDetails = "http://monteurverbund.com/mobile/updateAdminDetails";
    public static final String createUser = "http://monteurverbund.com/mobile/createUser";
    public static final String getUsers = "http://monteurverbund.com/mobile/getUsers";
    public static final String getDate = "http://monteurverbund.com/mobile/getDate";
    public static final String getPhotos = "http://monteurverbund.com/mobile/getPhotos";
    public static final String userLogin = "http://monteurverbund.com/mobile/userLogin";
    public static final String deletePhoto = "http://monteurverbund.com/mobile/deletePhoto";
    public static final String addPhoto = "http://monteurverbund.com/mobile/addPhoto";

    public static void clickEffect(final View view){
        view.setAlpha(0.5f);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setAlpha(1f);
            }
        }, 100);
    }
}
