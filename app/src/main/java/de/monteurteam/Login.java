package de.monteurteam;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.monteurteam.admin.AdminHome;
import de.monteurteam.network.CustomRequest;
import de.monteurteam.network.VolleyLibrary;
import de.monteurteam.user.UserHome;

import static de.monteurteam.Constants.adminLogin;

public class Login extends AppCompatActivity {
    @BindView(R.id.userImg)
    ImageView userImg;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.passwordImg)
    ImageView passwordImg;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.login)
    Button login;
    @BindView(R.id.forgotPassword)
    TextView forgotPassword;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.user)
    RadioButton user;
    @BindView(R.id.admin)
    RadioButton admin;
    @BindView(R.id.group)
    RadioGroup group;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        SharedPreferences sp = getSharedPreferences(Constants.sharedPrefs, Context.MODE_PRIVATE);
        if(sp.getInt("login", 0) == 1){
            if(sp.getInt("user_type", 0) == 0){
                Intent intent = new Intent(this, UserHome.class);
                startActivity(intent);
            }
            else{
                Intent intent = new Intent(this, AdminHome.class);
                startActivity(intent);
            }
            finish();
        }

        DrawableCompat.setTint(userImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));
        DrawableCompat.setTint(passwordImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneCall(Constants.adminNumber);
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
        }
    }

    @OnClick(R.id.login)
    public void onViewClicked() {
        Constants.clickEffect(login);
        if(name.getText().toString().isEmpty()){
            name.setError("");
            return;
        }
        if(password.getText().toString().isEmpty()){
            password.setError("");
            return;
        }

        if(group.getCheckedRadioButtonId() == R.id.user){
            loginUser();
        }
        else{
            loginAdmin();
        }
    }

    private void loginAdmin() {
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("email", name.getText().toString());
        hashMap.put("password", password.getText().toString());
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.adminLogin, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(progressBar == null)
                    return;
                progressBar.setVisibility(View.GONE);
                Log.e("success", ""+response.toString());
                try {
                    String status = response.getString("status");
                    if(status.equalsIgnoreCase("success")){
                        SharedPreferences sp = getSharedPreferences(Constants.sharedPrefs, Context.MODE_PRIVATE);
                        sp.edit().putInt("login", 1).apply();
                        sp.edit().putInt("user_type", 1).apply();
                        sp.edit().putString("user_id", "1").apply();
                        Intent intent = new Intent(Login.this, AdminHome.class);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        Toast.makeText(Login.this, "Failed to login!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", ""+error.toString());
                if(progressBar == null)
                    return;
                Toast.makeText(Login.this, "Network Problem!", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void loginUser() {
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("email", name.getText().toString());
        hashMap.put("password", password.getText().toString());
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.userLogin, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(progressBar == null)
                    return;
                progressBar.setVisibility(View.GONE);
                Log.e("success", response.toString());
                try {
                    String status = response.getString("status");
                    if(status.equalsIgnoreCase("success")){
                        SharedPreferences sp = getSharedPreferences(Constants.sharedPrefs, Context.MODE_PRIVATE);
                        sp.edit().putInt("login", 1).apply();
                        sp.edit().putInt("user_type", 0).apply();
                        sp.edit().putString("user_id", ""+response.getJSONObject("data").getInt("id")).apply();
                        sp.edit().putString("user_name", ""+response.getJSONObject("data").getString("name")).apply();
                        Intent intent = new Intent(Login.this, UserHome.class);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        Toast.makeText(Login.this, "Failed to login!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", ""+error.toString());
                if(progressBar == null)
                    return;
                Toast.makeText(Login.this, "Network Problem!", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    public void phoneCall(String number){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
            return;
        }
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:"+number));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            startActivity(intent);
        }
    }
}
