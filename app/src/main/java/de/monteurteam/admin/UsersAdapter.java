package de.monteurteam.admin;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.monteurteam.Constants;
import de.monteurteam.R;
import de.monteurteam.network.CustomRequest;
import de.monteurteam.network.VolleyLibrary;

public class UsersAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<UserModel> mList;
    private LayoutInflater mLayoutInflater = null;

    public UsersAdapter(Context context, ArrayList<UserModel> list) {
        mContext = context;
        mList = list;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int pos) {
        return mList.get(pos);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        CompleteListViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.user_item, null);
            viewHolder = new CompleteListViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (CompleteListViewHolder) v.getTag();
        }

        final UserModel model = mList.get(position);
        viewHolder.name.setText(model.getName());
        DrawableCompat.setTint(viewHolder.folderImg.getDrawable(), ContextCompat.getColor(mContext, R.color.icon_color));

        viewHolder.updatePanel.setVisibility(View.VISIBLE);
        DrawableCompat.setTint(viewHolder.editImg.getDrawable(), ContextCompat.getColor(mContext, R.color.icon_color));
        DrawableCompat.setTint(viewHolder.deleteImg.getDrawable(), ContextCompat.getColor(mContext, R.color.icon_color));

        viewHolder.editImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, AddUser.class);
                intent.putExtra("type", "edit");
                intent.putExtra("model", model);
                mContext.startActivity(intent);
            }
        });
        viewHolder.deleteImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert(mContext, model);
            }
        });

        return v;
    }

    private void showAlert(Context context, final UserModel model){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Delete User!")
            .setMessage("Are you sure you want to delete this user?")
            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    // delete user
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("name", model.getName());
                    hashMap.put("email", model.getEmail());
                    hashMap.put("password", model.getPassword());
                    hashMap.put("login", "0");
                    hashMap.put("mode", "delete");
                    hashMap.put("user_id", ""+model.getId());
                    CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.createUser, hashMap, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("success", response.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("error", "" + error.toString());
                            Toast.makeText(mContext, "Network Problem!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    VolleyLibrary.getInstance(mContext).addToRequestQueue(customRequest, "", false);
                    mList.remove(model);
                    notifyDataSetChanged();
                }
            })
            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // do nothing
                    dialog.dismiss();
                }
            })
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show();
    }

    static class CompleteListViewHolder {
        @BindView(R.id.folderImg)
        ImageView folderImg;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.editImg)
        ImageView editImg;
        @BindView(R.id.deleteImg)
        ImageView deleteImg;
        @BindView(R.id.updatePanel)
        LinearLayout updatePanel;

        public CompleteListViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
