package de.monteurteam.admin;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.monteurteam.Constants;
import de.monteurteam.R;
import de.monteurteam.network.CustomRequest;
import de.monteurteam.network.VolleyLibrary;

public class PhotosAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<PhotoModel> mList;
    private LayoutInflater mLayoutInflater = null;

    public PhotosAdapter(Context context, ArrayList<PhotoModel> list) {
        mContext = context;
        mList = list;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return mList.size();
    }
    @Override
    public Object getItem(int pos) {
        return mList.get(pos);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        CompleteListViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.photo_item, null);
            viewHolder = new CompleteListViewHolder(v);
            v.setTag(viewHolder);
        }
        else {
            viewHolder = (CompleteListViewHolder) v.getTag();
        }

        final PhotoModel model = mList.get(position);
        Glide.with(mContext).load(model.getPhoto()).into(viewHolder.imageView);

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert(mContext, model);
            }
        });

        return v;
    }

    private void showAlert(Context context, final PhotoModel model){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Delete Photo!")
                .setMessage("Are you sure you want to delete this photo?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // delete user
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("id", ""+model.getId());
                        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.deletePhoto, hashMap, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("success", response.toString());
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("error", "" + error.toString());
                                Toast.makeText(mContext, "Network Problem!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        VolleyLibrary.getInstance(mContext).addToRequestQueue(customRequest, "", false);
                        mList.remove(model);
                        notifyDataSetChanged();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    static class CompleteListViewHolder {
        // declare views here
        ImageView imageView;
        ImageView delete;
        public CompleteListViewHolder(View base) {
            //initialize views here
            imageView=base.findViewById(R.id.photo);
            delete=base.findViewById(R.id.delete);
        }
    }
}
