package de.monteurteam.admin;

import java.io.Serializable;

public class UserModel implements Serializable {
    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("login")
    private int login;
    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("password")
    private String password;
    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("email")
    private String email;
    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("name")
    private String name;
    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("id")
    private int id;

    public int getLogin() {
        return login;
    }

    public void setLogin(int login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
