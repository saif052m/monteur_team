package de.monteurteam.admin;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.monteurteam.R;

public class FoldersAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<FolderModel> mList;
    private LayoutInflater mLayoutInflater = null;

    public FoldersAdapter(Context context, ArrayList<FolderModel> list) {
        mContext = context;
        mList = list;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return mList.size();
    }
    @Override
    public Object getItem(int pos) {
        return mList.get(pos);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        CompleteListViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.user_item, null);
            viewHolder = new CompleteListViewHolder(v);
            v.setTag(viewHolder);
        }
        else {
            viewHolder = (CompleteListViewHolder) v.getTag();
        }

        FolderModel model = mList.get(position);
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(model.getDate());
            viewHolder.name.setText(new SimpleDateFormat("dd-MM-yyyy").format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            viewHolder.name.setText(model.getDate());
        }
        DrawableCompat.setTint(viewHolder.folderImg.getDrawable(), ContextCompat.getColor(mContext, R.color.icon_color));

        return v;
    }

    static class CompleteListViewHolder {
        @BindView(R.id.folderImg)
        ImageView folderImg;
        @BindView(R.id.name)
        TextView name;
        public CompleteListViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
