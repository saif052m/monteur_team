package de.monteurteam.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.monteurteam.Constants;
import de.monteurteam.R;
import de.monteurteam.network.CustomRequest;
import de.monteurteam.network.VolleyLibrary;

public class AllUsers extends AppCompatActivity {
    @BindView(R.id.titleBar)
    LinearLayout titleBar;
    @BindView(R.id.listView)
    ListView listView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.noresult)
    TextView noresult;
    @BindView(R.id.cloud)
    ImageView cloud;
    ArrayList<UserModel> models = new ArrayList<>();
    UsersAdapter usersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_users);

        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllUsers();
    }

    private void getAllUsers() {
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> hashMap = new HashMap<>();
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.getUsers, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (progressBar == null)
                    return;
                progressBar.setVisibility(View.GONE);
                models.clear();
                Log.e("success", response.toString());
                try {
                    String status = response.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonArray = response.getJSONArray("data");
                        Type listType = new TypeToken<ArrayList<UserModel>>(){}.getType();
                        ArrayList<UserModel> modelsNew =  new GsonBuilder().create().fromJson(jsonArray.toString(), listType);
                        models.addAll(modelsNew);

                        if(models.size() == 0)
                            noresult.setVisibility(View.VISIBLE);
                        else
                            noresult.setVisibility(View.GONE);

                        usersAdapter=new UsersAdapter(AllUsers.this, models);
                        listView.setAdapter(usersAdapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                Intent intent = new Intent(AllUsers.this, AllFolders.class);
                                intent.putExtra("model", models.get(pos));
                                startActivity(intent);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "" + error.toString());
                if (progressBar == null)
                    return;
                Toast.makeText(AllUsers.this, "Network Problem!", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }
}
