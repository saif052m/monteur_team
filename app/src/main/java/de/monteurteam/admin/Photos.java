package de.monteurteam.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.monteurteam.Constants;
import de.monteurteam.R;
import de.monteurteam.ViewPhoto;
import de.monteurteam.network.CustomRequest;
import de.monteurteam.network.VolleyLibrary;

public class Photos extends AppCompatActivity {

    @BindView(R.id.titleBar)
    LinearLayout titleBar;
    @BindView(R.id.listView)
    GridView gridView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.noresult)
    TextView noresult;
    @BindView(R.id.cloud)
    ImageView cloud;
    ArrayList<PhotoModel> models = new ArrayList<>();
    PhotosAdapter photosAdapter;
    FolderModel folderModel;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);
        ButterKnife.bind(this);

        folderModel = (FolderModel) getIntent().getSerializableExtra("model");
        userId = getIntent().getStringExtra("user_id");
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPhotos();
    }

    private void getPhotos() {
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", ""+userId);
        hashMap.put("date", ""+folderModel.getDate());
        Log.e("tag", "id: "+userId);
        Log.e("tag", "date: "+folderModel.getDate());
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.getPhotos, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (progressBar == null)
                    return;
                progressBar.setVisibility(View.GONE);
                Log.e("success", response.toString());
                models.clear();
                try {
                    String status = response.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonArray = response.getJSONArray("data");
                        Type listType = new TypeToken<ArrayList<PhotoModel>>(){}.getType();
                        ArrayList<PhotoModel> modelsNew =  new GsonBuilder().create().fromJson(jsonArray.toString(), listType);
                        models.addAll(modelsNew);

                        if(models.size() == 0)
                            noresult.setVisibility(View.VISIBLE);
                        else
                            noresult.setVisibility(View.GONE);

                        photosAdapter = new PhotosAdapter(Photos.this, models);
                        gridView.setAdapter(photosAdapter);
                        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                Intent intent = new Intent(Photos.this, ViewPhoto.class);
                                intent.putExtra("url", models.get(pos).getPhoto());
                                startActivity(intent);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "" + error.toString());
                if (progressBar == null)
                    return;
                Toast.makeText(Photos.this, "Network Problem!", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }
}
