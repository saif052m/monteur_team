package de.monteurteam.admin;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.monteurteam.Constants;
import de.monteurteam.Login;
import de.monteurteam.R;

public class AdminHome extends AppCompatActivity {

    @BindView(R.id.userImg)
    ImageView userImg;
    @BindView(R.id.allUsers)
    TextView allUsers;
    @BindView(R.id.userPanel)
    LinearLayout userPanel;
    @BindView(R.id.addImg)
    ImageView addImg;
    @BindView(R.id.addUser)
    TextView addUser;
    @BindView(R.id.addPanel)
    LinearLayout addPanel;
    @BindView(R.id.logoutImg)
    ImageView logoutImg;
    @BindView(R.id.logout)
    TextView logout;
    @BindView(R.id.logoutPanel)
    LinearLayout logoutPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);
        ButterKnife.bind(this);

        DrawableCompat.setTint(addImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));
        DrawableCompat.setTint(userImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));
        DrawableCompat.setTint(logoutImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
        }
    }

    @OnClick({R.id.userPanel, R.id.addPanel, R.id.logoutPanel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.userPanel:
                Intent intent = new Intent(this, AllUsers.class);
                startActivity(intent);
                break;
            case R.id.addPanel:
                Intent intent1 = new Intent(this, AddUser.class);
                intent1.putExtra("type", "add");
                startActivity(intent1);
                break;
            case R.id.logoutPanel:
                SharedPreferences sp = getSharedPreferences(Constants.sharedPrefs, Context.MODE_PRIVATE);
                sp.edit().putInt("login", 0).apply();
                Intent intent2 = new Intent(this, Login.class);
                startActivity(intent2);
                finish();
                break;
        }
    }
}
