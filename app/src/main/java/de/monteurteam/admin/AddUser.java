package de.monteurteam.admin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.monteurteam.Constants;
import de.monteurteam.Login;
import de.monteurteam.R;
import de.monteurteam.network.CustomRequest;
import de.monteurteam.network.VolleyLibrary;
import de.monteurteam.user.UserHome;

public class AddUser extends AppCompatActivity {
    @BindView(R.id.userImg)
    ImageView userImg;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.passwordImg)
    ImageView passwordImg;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.register)
    Button register;
    UserModel userModel;
    String type;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.emailImg)
    ImageView emailImg;
    @BindView(R.id.email)
    EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);

        DrawableCompat.setTint(userImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));
        DrawableCompat.setTint(emailImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));
        DrawableCompat.setTint(passwordImg.getDrawable(), ContextCompat.getColor(this, R.color.icon_color));

        type = getIntent().getStringExtra("type");
        userModel = (UserModel) getIntent().getSerializableExtra("model");

        if (type.equalsIgnoreCase("add")) {
            register.setText("Register");
        } else {
            register.setText("Update");
            // set data
            name.setText(userModel.getName());
            email.setText(userModel.getEmail());
            password.setText(userModel.getPassword());
        }

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equalsIgnoreCase("add")) {
                    addUser();
                } else {
                    updateUser();
                }
            }
        });
    }

    private void updateUser() {
        if (name.getText().toString().isEmpty()) {
            name.setError("");
            return;
        }
        if (email.getText().toString().isEmpty()) {
            email.setError("");
            return;
        }
        if (password.getText().toString().isEmpty()) {
            password.setError("");
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("name", name.getText().toString());
        hashMap.put("email", email.getText().toString());
        hashMap.put("password", password.getText().toString());
        hashMap.put("login", "0");
        hashMap.put("mode", "update");
        hashMap.put("user_id", ""+userModel.getId());
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.createUser, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (progressBar == null)
                    return;
                progressBar.setVisibility(View.GONE);
                Log.e("success", response.toString());
                try {
                    String status = response.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        Toast.makeText(AddUser.this, "User updated!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else {
                        Toast.makeText(AddUser.this, "Failed to update!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "" + error.toString());
                if (progressBar == null)
                    return;
                Toast.makeText(AddUser.this, "Network Problem!", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void addUser() {
        if (name.getText().toString().isEmpty()) {
            name.setError("");
            return;
        }
        if (email.getText().toString().isEmpty()) {
            email.setError("");
            return;
        }
        if (password.getText().toString().isEmpty()) {
            password.setError("");
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("name", name.getText().toString());
        hashMap.put("email", email.getText().toString());
        hashMap.put("password", password.getText().toString());
        hashMap.put("login", "0");
        hashMap.put("mode", "create");
        hashMap.put("user_id", "");
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.createUser, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (progressBar == null)
                    return;
                progressBar.setVisibility(View.GONE);
                Log.e("success", response.toString());
                try {
                    String status = response.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        Toast.makeText(AddUser.this, "User created!", Toast.LENGTH_SHORT).show();
                        name.setText("");
                        email.setText("");
                        password.setText("");
                    }
                    else {
                        Toast.makeText(AddUser.this, "Failed!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "" + error.toString());
                if (progressBar == null)
                    return;
                Toast.makeText(AddUser.this, "Network Problem!", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }
}
