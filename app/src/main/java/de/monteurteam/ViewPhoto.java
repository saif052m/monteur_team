package de.monteurteam;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.jsibbold.zoomage.ZoomageView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewPhoto extends AppCompatActivity {
    @BindView(R.id.loading)
    TextView loading;
    @BindView(R.id.photo)
    ZoomageView photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_photo);
        ButterKnife.bind(this);

        Toast.makeText(ViewPhoto.this, "Loading Photo...", Toast.LENGTH_SHORT).show();

        String url = getIntent().getStringExtra("url");
        if(url != null && !url.isEmpty()) {
            Glide.with(this).load(url).into(photo);
        }

    }

}
